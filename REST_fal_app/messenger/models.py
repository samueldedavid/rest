from mongoengine import *
import datetime
import sys


class message(Document):
    message = StringField(max_length=200, required=True)
    time = DateTimeField(required=True,default=datetime.datetime.utcnow)
class StorageEngine(object):
    def __init__(self):
        if "pytest" in sys.modules:
            print("running pytest")
            self.db = connect('test')
            self.db.drop_database('test')
        else:
            self.db = connect('restDB')

    def get_all_messages(self ):
        return message.objects().only('message').to_json()

    def get_message(self , msg:str):
        to_del=message.objects(message=msg).only('message').first()
        if to_del != None:
            x = to_del.message
            return x 
        return None

    def add_message(self , msg:str):

        mdb=message(msg)
        mdb.save()
        return mdb.id

    def delete_message(self , msg:str):
        to_del=message.objects(message=msg).only('message').first()
        if to_del != None:
            x= to_del.message
            to_del.delete()
            return x 
        return None

    

    