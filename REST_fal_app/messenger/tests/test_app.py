# -----------------------------------------------------------------
# pytest
# -----------------------------------------------------------------

from falcon import testing
import pytest
import json
from messenger.app import api
test_messages=dict()

# Depending on your testing strategy and how your application
# manages state, you may be able to broaden the fixture scope
# beyond the default 'function' scope used in this example.

@pytest.fixture()
def client():
    
    test_messages['messages']=list()
    test_messages['messages_palindrome']=list()
    for i in range(0 ,6):
        if i < 4:
           test_messages['messages'].append('!! Sending Message{0} hope its received!!'.format(i))
        else:
            test_messages['messages'].append('')   
    for i in range(0 ,6):
        if i < 4:
           test_messages['messages_palindrome'].append('!! {0}Message{1} !!'.format(i,i))
        else:
            test_messages['messages_palindrome'].append('this is not a palindrome ') 
            
    return testing.TestClient(api)



def test_post_message(client):
    for k , v in test_messages.items():
            for msg in v:
                doc = json.dumps({'message':msg})

                result = client.simulate_post('/messages',body=doc)
        
                assert result.json['message'] == msg
        



                
      
        
def test_get_message(client):
    

    doc = json.dumps(test_messages)

    result = client.simulate_get('/messages',body=doc)
    messages_list = json.loads(result.json)
    kv=list()
    kv.extend(test_messages['messages'])
    kv.extend(test_messages['messages_palindrome'])
    for rmsgi ,rmsg in enumerate(messages_list):
        assert messages_list[rmsgi]['message'] in kv , '\n{0} is not in list with contents: \n{1}'.format(messages_list[rmsgi]['message'] ,kv)

def test_get_message_palindrome(client):
        
        doc = json.dumps({'message':test_messages['messages_palindrome'][4]})

        result = client.simulate_get('/messages/palindrome',body=doc)

        assert result.json['palindrome'] == 'False'
    
        

def test_delete_message(client):
    

    
    for k , v in test_messages.items():
        for msg in v:    
                doc = json.dumps({'message':msg})

                result = client.simulate_delete('/messages',body=doc)
                # print(doc,"   " ,result.json)
                assert result.json['message'] == msg   
    for k , v in test_messages.items():
        for msg in v :         
                doc = json.dumps({'message':msg})

                result = client.simulate_delete('/messages',body=doc)
                # print(result.json)
                assert result.json['message'] == 'None'

        