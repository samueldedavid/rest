import falcon
from resources import resource_message , resource_palindrome
from models import StorageEngine
def create_app():
    db = StorageEngine()
    api = falcon.API()
    api.req_options.auto_parse_form_urlencoded=True
    messages = resource_message(db)
    pal = resource_palindrome(db)
    api.add_route('/messages',messages)
    api.add_route('/messages',messages)
    api.add_route('/messages/palindrome',pal)
    return api

api = create_app()


if __name__ == "__main__":
    from waitress import serve
    serve(api, port='8080')