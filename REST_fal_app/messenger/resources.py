import json
import falcon
import datetime
from models import StorageEngine

class resource_message(object):
    def __init__(self, db):
        self.db = db
    def on_post(self,req,resp):
        '''
        This function is the basic Falcon api POST callback for posting messages
        the resource messenger class gives a JSON object of the format 
        Example :

        {'message': '{ MSG }'}
        
        MSG: Actual Message submitted/posted
        
        '''
        data=json.loads(req.stream.read())
        b=(data['message'])
        id = self.db.add_message(b)
        output = {
                'messageId':'{0}'.format(id)
                ,
                'message':b
            }
        resp.body = json.dumps(output)
        resp.status = falcon.HTTP_201

    def on_get(self,req,resp):
        '''
        This function is the basic Falcon api GET callback for getting received messages 
        the resource messenger class gives a JSON array of objects of the format 
        Example :

        ['message': '{Actual Message posted on the server}'...,...,...]

        OR

        {'message':''} Empty message.
        
        '''
        resp.status = falcon.HTTP_200
        
        output = self.db.get_all_messages()
        resp.body = json.dumps(
        
               output 
            )  

    def on_delete(self,req,resp):
        '''
        This function is the basic Falcon api DELETE callback to delete message
        with contents == MSG
        the resource messenger class gives a JSON array of the format 
        Example :

        {'message': '{MSG}'}
        MSG: Actual Message posted on the server ready to be deleted

        OR

        {'message':'None' } 

        NotFound: Not found message which tells if the 
        message is already deleted or not found .
        
        '''
        resp.status = falcon.HTTP_200

        data=json.loads(req.stream.read())
        qry=(data['message'])

        to_del=self.db.delete_message(qry)
        if (to_del)!=None:
            output={"message" : to_del}
            resp.status = falcon.HTTP_202
        else:
            output={'message':'None'}
            resp.status = falcon.HTTP_404
        
        resp.body = json.dumps(
        
               output
            )        
class resource_palindrome(object):
    def __init__(self, db):
        self.db = db
    def on_get(self,req,resp):
        '''
        This function is the basic Falcon api POST callback to check message
        with contents == MSG
        the resource messenger class gives a JSON array of the format 
        Example :

        {'message': '{MSG}' , 'palindrome' : {bool} }
        MSG: Actual Message posted on the server ready to be deleted

        OR

        {'message':'None' } 

        NotFound: Not found message which tells if the 
        message is already deleted or not found .
        
        '''
        resp.status = falcon.HTTP_200

        data=json.loads(req.stream.read())
        qry=data['message']
        to_check=self.db.get_message(qry)
        if (to_check)!=None:
            print('to check',to_check)
            output={"message" : to_check,"palindrome" : "{0}".format(self.is_palindrome(to_check))}
        else:
            output={'message':'None','palindrome':'0'}
            resp.status = falcon.HTTP_404

        
        resp.body = json.dumps(
            
                output
            ) 
    def is_palindrome(self,message : str) -> bool:
        """

        Checks if a sentence  is a palidrome based on 
        words in a sentence by lowercasing it first .
        Example : 
        for a collection of sentences
        sentences = [ "Hello","asdf saf as fdsfasdfe as",
                    "This is is This",
                    "This is This","This is THIS"
                ]
        It will produce 
        True
        True
        False
        True
        True
        True
        
        """
        length = len(message)
        if length<=0:
            return True
        message_list = message.lower().split()
        for i in range (0,int(length/2)):
            if message_list[i]!=message_list[-(i+1)]:
                return False
            else:
                return True           



