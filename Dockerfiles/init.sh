#!/bin/bash
mongod --fork --logpath /var/log/mongodb/mongodb.log
git clone  https://samueldedavid@bitbucket.org/samueldedavid/rest.git
echo pip3
pip3 install falcon==1.4.1
pip3 install mongoengine==0.16.3
pip3 install pytest==4.3.0
pip3 install waitress==1.2.1
echo app
# cd rest/REST_fal_app/messenger
python3 rest/REST_fal_app/messenger/app.py