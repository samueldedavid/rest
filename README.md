# REST
It is a flacon based REST api
with following Endpoints 
It sends and receives the data by JSON objects in the
body . It doesnot use query parameters for querying and 
stores the data in mongdb with a length of max 200 . 


## POST /messages

response body

    {'message': '{ MSG }'}
MSG: Actual Message submitted/posted

## GET /messages 
response body


    ['message': '{Actual Message posted on the server}'...,...,...]
OR
    
    [] Empty  


## DELETE /messages
response body

    {'message': '{MSG}'}
MSG: Actual Message posted on the server ready to be deleted
    OR

    {'message':'None' }

## POST /messages/palindrome
response body
    
    {'message': '{MSG}' , 'palindrome' : {bool} }

MSG: Actual Message posted on the server ready to be deleted
OR
q
    {'message':'None' } 

# Running
## Prerequisites
It was run on 
* python 3.6 and 
* using mongodb server 4.0.6 
* [falcon api](https://falcon.readthedocs.io/en/stable/) inline link.
* conda package manager 
* pytest
## Pytests
Pytests was used for functional testing 
In order to check that run it in tthe  dir where app.py is located
which is rest/REST_fal_app/messenger 

    python -m pytest
    
## local-server
Its runs on local server by runnig the command 
    
    
    python app.py 
## Local docker container
    docker build -f abledear/pm 
## Issues 
It runs fine on local server config but when running in continer the json.loads throws error that it needs str and not bytes which can be due to the fact that it uses 3.5 in the docker container 



